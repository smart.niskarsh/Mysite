var request = require('request');
var location, latitude, longitude;
request.get({
  url:"http://ipinfo.io",
  json:true
},
function(error,response, body) { 
  latitude = parseFloat((body.loc).substring(0,(body.loc).indexOf(",")));
  longitude = parseFloat((body.loc).substring((body.loc).indexOf(",")+1));
  location = {
    lat:latitude,
    lon:longitude
  };
  console.log(location);
  exports=module.exports=location;
});